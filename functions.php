<?php
/**
 * On peut déclarer des fonctions hors de toute classe comme en JS
 */
function multiply(int $a, int $b): int
{
    return $a * $b;
}
