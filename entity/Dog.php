<?php

/**
 * Les classes PHP fonctionne de la même manière qu'en Java et sont assez proche en terme 
 * de syntaxe
 */
class Dog {
    /**
     * On peut typer ou pas les propriétés d'une classe (depuis la version 7), je
     * conseil de le faire quand c'est possible
     */
    private int $id;
    private string $name;
    private string $breed;
    private DateTime $birthdate;

    /**
     * La méthode __construct représente le constructeur de la classe, il se rapproche
     * de la façon dont ça marche en javascript, à savoir qu'un seul constructeur par
     * classe est possible, avec éventuellement des paramètres par défaut
     */
    public function __construct(int $id, string $name, string $breed, DateTime $birthdate) {
        $this->id = $id;
        $this->name = $name;
        $this->breed = $breed;
        $this->birthdate = $birthdate;
    }

    /**
     * Les méthodes se rapprochent de ce que l'on fait en Java ou TS, juste bien
     * mettre le mot clef function, mais sinon le principe est le même, on type le
     * retour (ou pas), on type les arguments (ou pas)
     */
    public function getId():int {
        return $this->id;
    }

    public function setId(int $id):void {
        $this->id = $id;
    }

}