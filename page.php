<?php 
//Ici on récupère notre fragment de page header.php et on le charge directement
//à cet endroit de la page actuelle
require_once 'template-parts/header.php';

//exemple d'utilisation de POO
require_once 'entity/Dog.php';

$dog = new Dog(1, "fido", "corgi", new DateTime());

echo '<pre>';
var_dump($dog);
echo '</pre>';
?>


<h1>Salut</h1>

<p>autre chose</p>

<button class="btn btn-primary">test</button>


<?php
//pareil que le header mais pour le footer
require_once 'template-parts/footer.php';

?>